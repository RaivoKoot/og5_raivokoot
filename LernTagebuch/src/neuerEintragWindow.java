import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class neuerEintragWindow extends JFrame {
	private Hauptfenster meinHauptfenster;

	public neuerEintragWindow(Hauptfenster hf) {
		meinHauptfenster = hf;

		setTitle("Neuer Eintrag");
		setLayout(new GridLayout(0, 2));

		JLabel lblDatum = new JLabel("Datum:");
		JLabel lblFach = new JLabel("Fach:");
		JLabel lblBeschreibung = new JLabel("Inhalt");
		JLabel lblDauer = new JLabel("Dauer:");

		JTextField tfDatum = new JTextField("TT.MM.JJJJ");
		JTextField tfFach = new JTextField();
		JTextField tfBeschreibung = new JTextField();
		JTextField tfDauer = new JTextField();

		add(lblDatum);
		add(tfDatum);
		add(lblFach);
		add(tfFach);
		add(lblBeschreibung);
		add(tfBeschreibung);
		add(lblDauer);
		add(tfDauer);

		JButton btnSpeichern = new JButton("Speichern");
		JButton btnAbbrechen = new JButton("Abbrechen");
		add(btnSpeichern);
		add(btnAbbrechen);

		btnAbbrechen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});

		btnSpeichern.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String datum = tfDatum.getText();
				String fach = tfFach.getText();
				String beschreibung = tfBeschreibung.getText();
				String dauer = tfDauer.getText();
				LernEintrag neuerLernEintrag = new LernEintrag(datum, fach, beschreibung, dauer);

				meinHauptfenster.getLogic().getFileControl().eintragEinschreiben(neuerLernEintrag);
				meinHauptfenster.getLogic().reloadFromDisk();
				meinHauptfenster.eintraegeAnzeigen();
				dispose();
			}
		});

		pack();
		setVisible(true);
	}

}
