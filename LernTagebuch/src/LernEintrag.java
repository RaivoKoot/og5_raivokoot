import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

public class LernEintrag {

	Date datum;
	String fach;
	String beschreibung;
	int dauer;

	public LernEintrag(String date, String f, String b, String d) {
		try {
			datum = DateFormat.getDateInstance().parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		fach = f;
		beschreibung = b;
		dauer = Integer.parseInt(d);
	}

	public void setDatum(Date d) {
		datum = d;
	}

	public String getDatum() {
		return DateFormat.getDateInstance().format(datum);
	}

	public void setFach(String f) {
		fach = f;
	}

	public String getFach() {
		return fach;
	}

	public void setBeschreibung(String b) {
		beschreibung = b;
	}

	public String getBeschreibung() {
		return beschreibung;
	}

	public void setDauer(int d) {
		dauer = d;
	}

	public String getDauer() {
		return String.valueOf(dauer);
	}

	public String toString() {
		return String.format("%-10s %-11s %-40s %-10s %n", DateFormat.getDateInstance().format(datum), "|" + fach,
				"|" + beschreibung, "|" + dauer);
	}
}
