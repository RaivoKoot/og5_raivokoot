import java.util.List;

public class Logic {

	public FileControl fc;
	private List<LernEintrag> eintraege;

	public Logic(FileControl f) {
		fc = f;
	}

	public FileControl getFileControl() {
		return fc;
	}

	public void reloadFromDisk() {
		eintraege = fc.dateiEinlesenUndAusgeben();
	}

	public List<LernEintrag> getListEntries() {
		return eintraege;
	}

	public String getLearnerName() {
		return fc.getName();
	}

	public String createReport() {
		String report = "";
		int gesamtDauer = 0;

		for (LernEintrag le : eintraege) {
			gesamtDauer += Integer.parseInt(le.getDauer());
		}
		int durchschnittsDauer = gesamtDauer / eintraege.size();

		report += "\nAnzahl Eintraege: " + eintraege.size();
		report += "\nInsgesamt gelernt: " + gesamtDauer;
		report += "\nDurchschnittsdauer pro Lern Session: " + durchschnittsDauer;
		return report;
	}

}
