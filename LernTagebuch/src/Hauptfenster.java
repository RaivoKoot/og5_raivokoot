import java.awt.*;
import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Hauptfenster extends JFrame implements ActionListener {
	private JButton neuerEintrag;
	public Logic meinLogic;
	private TextArea eintragDisplay;
	private JButton bericht;

	public Logic getLogic() {
		return meinLogic;
	}

	public Hauptfenster(Logic lgc) {
		meinLogic = lgc;
		meinLogic.reloadFromDisk();

		setLayout(new BorderLayout());
		Container c = getContentPane();

		JPanel kopfZeile = new JPanel();
		kopfZeile.setLayout(new FlowLayout(FlowLayout.LEFT));
		kopfZeile.add(new JLabel(meinLogic.getLearnerName()));
		c.add(kopfZeile, BorderLayout.NORTH);

		JPanel unterZeile = new JPanel();
		unterZeile.setLayout(new FlowLayout(FlowLayout.RIGHT));
		neuerEintrag = new JButton("Neuer Eintrag...");
		bericht = new JButton("Bericht...");
		bericht.addActionListener(this);
		JButton beenden = new JButton("Beenden");
		beenden.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});

		neuerEintrag.addActionListener(this);
		unterZeile.add(neuerEintrag);
		unterZeile.add(bericht);
		unterZeile.add(beenden);
		c.add(unterZeile, BorderLayout.SOUTH);

		JPanel zentralEbene = new JPanel();
		zentralEbene.setLayout(new FlowLayout());
		eintragDisplay = new TextArea("", 10, 78, TextArea.SCROLLBARS_VERTICAL_ONLY);
		eintragDisplay.setFont(new Font("monospaced", Font.PLAIN, 12));
		eintragDisplay.setEditable(false);
		eintragDisplay.setFocusable(false);
		eintraegeAnzeigen();

		zentralEbene.add(eintragDisplay);
		c.add(zentralEbene, BorderLayout.CENTER);

		setTitle("Lerntagebuch");
		pack();
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				new Hauptfenster(new Logic(new FileControl())).setVisible(true);
			}
		});
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource() == neuerEintrag)
			new neuerEintragWindow(this);
		else if (e.getSource() == bericht)
			new berichtWindow(this);

	}

	public void eintraegeAnzeigen() {
		eintragDisplay.setText("");
		eintragDisplay.append(String.format("%-10s %-11s %-40s %-10s %n", "Datum", "|Fach", "|Aktivitaet", "|Dauer"));
		eintragDisplay.append("---------------------------------------------------------------------------\n");
		for (LernEintrag eintrag : meinLogic.getListEntries())
			eintragDisplay.append(eintrag.toString());
	}
}
