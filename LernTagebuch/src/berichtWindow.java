import java.awt.*;

import javax.swing.*;

public class berichtWindow extends JFrame {

	private Hauptfenster meinHauptfenster;

	public berichtWindow(Hauptfenster hf) {
		meinHauptfenster = hf;
		setTitle("Bericht");
		setLayout(new BorderLayout());

		String headerString = "Lernbericht fuer " + meinHauptfenster.getLogic().getLearnerName();
		JLabel header = new JLabel(headerString);
		header.setBorder(BorderFactory.createEmptyBorder(10,55,10,10));
		add(header,BorderLayout.NORTH);

		JPanel hauptPanel = new JPanel();
		hauptPanel.setLayout(new FlowLayout());

		JTextArea berichtArea = new JTextArea(headerString, 0, 0);
		berichtArea.append("\n-------------------------------------------------");
		berichtArea.append(meinHauptfenster.getLogic().createReport());
		berichtArea.setEditable(false);
		berichtArea.setFocusable(false);
		berichtArea.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));

		add(berichtArea,BorderLayout.CENTER);
	//	add(hauptPanel, BorderLayout.CENTER);

		pack();
		setVisible(true);

	}

}
