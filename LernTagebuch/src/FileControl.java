import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FileControl {

	File file = new File("miriam.dat");
	FileReader fr;
	BufferedReader br;
	String name = "name uninitialized";

	FileWriter fw;
	BufferedWriter bw;

	public String getName() {
		return name;
	}

	public List<LernEintrag> dateiEinlesenUndAusgeben() {
		List<LernEintrag> eintraege = new ArrayList<LernEintrag>();

		try {
			fr = new FileReader(file);
			br = new BufferedReader(fr);

			name = br.readLine();
			String s;

			String datum = "";
			String fach = "";
			String aktivitaet = "";
			String dauer = "";
			int counter = 0;

			while ((s = br.readLine()) != null) {

				switch (counter) { // @formatter:off
				case 0: datum = s; break;
				case 1: fach = s; break;
				case 2: aktivitaet = s; break;
				case 3: dauer = s; break;
				} // @formatter:on
				counter++;
				
				if (counter == 4) {
					eintraege.add(new LernEintrag(datum, fach, aktivitaet, dauer));
					counter = 0;
				}
			}

			br.close();
		} catch (FileNotFoundException e) {
			System.out.println("FileNotFoundException: " + e.getMessage());
		} catch (IOException e) {
			System.out.println("IOException: " + e.getMessage());
		}
		return eintraege;
	}

	public void eintragEinschreiben(LernEintrag neuerEintrag) {
		try {
			fw = new FileWriter(file, true);
			bw = new BufferedWriter(fw);

			bw.newLine();
			bw.write(neuerEintrag.getDatum());
			
			bw.newLine();
			bw.write(neuerEintrag.getFach());
			
			bw.newLine();
			bw.write(neuerEintrag.getBeschreibung());
			
			bw.newLine();
			bw.write(neuerEintrag.getDauer());
			
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
