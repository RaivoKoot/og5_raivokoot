import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class ParcourAnalyse {
	private static final int INKREMENT_WERT = 1000000000;
	
	public static void main(String[] args) {
		ParcourAnalyse test = new ParcourAnalyse();
		test.kursAnalyse();
	}

	public boolean kursAnalyse() {
		// Attribute zum Lesen der Datei
		Scanner input = new Scanner(System.in);
		System.out.println("Dateipfad angeben: ");
		String filePath = input.nextLine();
		input.close();

		File testFile = new File(filePath);
		FileReader fileReader;
		BufferedReader buffReader;
		int nextTrack;

		// gebraucht fuer das Verfahren des ersten Durchlaufs
		long differenz = 0;
		int mindestSpeed = 0;
		int geradenGesamt = 0;
		try {
			fileReader = new FileReader(testFile);
			buffReader = new BufferedReader(fileReader);

			// neuen Charakter lesen bis -1 (ende) erreicht ist
			while ((nextTrack = buffReader.read()) != -1) {
				// charakter wird in unicode ausgelesen
				switch (nextTrack) {
				case 92: // case bergab
					differenz += INKREMENT_WERT;
					mindestSpeed++;
					break;
				case 95: // case Gerade
					differenz++;
					mindestSpeed--;
					geradenGesamt++;
					break;
				case 47: // case bergauf
					if (differenz < INKREMENT_WERT)
						differenz--;
					else
						differenz -= INKREMENT_WERT;
					mindestSpeed--;
					break;
				}
				// Mindestgeschwindigkeit darf nicht utner 0
				if (mindestSpeed < 0)
					mindestSpeed = 0;
				// abbruchbedingung
				if (differenz < 0) {
					fileReader.close();
					buffReader.close();
					System.out.print("Befahrbar: false");
					return false;
				}
			}

			// Exception Handling und Streams schliessen
			fileReader.close();
			buffReader.close();
		} catch (FileNotFoundException e) {
			System.err.println("FileNotFoundException: " + e.getMessage());
		} catch (IOException e) {
			System.err.println("IOException: " + e.getMessage());
		}
		// ob befahrbar ist ausgeben
		if (mindestSpeed == 0 && differenz % INKREMENT_WERT >= differenz / INKREMENT_WERT
				&& (differenz % INKREMENT_WERT - differenz / INKREMENT_WERT) % 2 == 0) {
			System.out.println(anweisungen(differenz, geradenGesamt));
			System.out.print("Befahrbar: true");
			return true;
		} else {
			System.out.print("Befahrbar: false");
			return false;
		}
	}

	private String anweisungen(long differenz, int geradenGesamt) {
		int x = (int) (differenz % INKREMENT_WERT); // geradenwert
		int y = (int) (differenz / INKREMENT_WERT); // Miliardenanzahl

		long beschleunigungen = (x - y) / 2 + geradenGesamt - x;
		String anweisung = "Du musst die ersten " + beschleunigungen + " Geraden beschleunigen";
		anweisung += ". Danach nur noch langsamer werden.";
		return anweisung;
	}
}




