package de.futurehome.tanksimulator;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import static java.lang.String.format;

public class MyActionListener implements ActionListener {
	public TankSimulator f;

	public MyActionListener(TankSimulator f) {
		this.f = f;
	}

	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		if (obj == f.btnBeenden)
			System.exit(0);

		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		String logDate = dateFormat.format(date);
		String logAktion;
		String logFuellstand;
		String logEintrag;

		if (obj == f.btnEinfuellen) {
			double fuellstand = f.myTank.getFuellstand();

			// neuen Logeintrag nur erstellen
			// wenn fuellstand nicht schon 100 ist
			if (fuellstand < 100) {
				fuellstand += 5;
				if (fuellstand > 100) {
					fuellstand = 100.0;
				}
				// log
				logAktion = "Tank aufgefuellt";
				logFuellstand = "Fuellstand: " + fuellstand;
				logEintrag = String.format("%-30s %-30s %30s", logDate, logAktion, logFuellstand);
				f.txaLog.append(logEintrag + "\n");
			}

			f.myTank.setFuellstand(fuellstand);
			f.lblFuellstand.setText("" + fuellstand);
			f.progressBar.setValue((int) fuellstand);
		}

		if (obj == f.btnVerbrauchen) {
			double fuellstand = f.myTank.getFuellstand();

			// neuen Logeintrag nur erstellen
			// wenn fuellstand nicht schon 0 ist
			if (fuellstand > 0) {
				fuellstand -= 2;
				if (fuellstand < 0) {
					fuellstand = 0;
				}
				// log
				logAktion = "Tank entnommen";
				logFuellstand = "Fuellstand: " + fuellstand;
				logEintrag = String.format("%-30s %-25s %30s", logDate, logAktion, logFuellstand);
				f.txaLog.append(logEintrag + "\n");
			}

			f.myTank.setFuellstand(fuellstand);

			f.lblFuellstand.setText("" + fuellstand);
			f.progressBar.setValue((int) fuellstand);
		}

		if (obj == f.btnZuruecksetzen) {
			double fuellstand = f.myTank.getFuellstand();
			// neuen Logeintrag nur erstellen
			// wenn fuellstand nicht schon 0 ist
			if (fuellstand > 0) {
				logAktion = "Tank geleert";
				logFuellstand = "Fuellstand: " + 0;
				logEintrag = String.format("%-30s %-36s %25s", logDate, logAktion, logFuellstand);
				f.txaLog.append(logEintrag + "\n");
			}

			f.myTank.setFuellstand(0.0);
			f.lblFuellstand.setText("" + 0.0);
			f.progressBar.setValue(0);
		}

	}
}