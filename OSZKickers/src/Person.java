
public abstract class Person {

	private String name;
	private String tele;
	private boolean hatBezahlt;
	
	public String getName(){
		return name;
	}
	
	public void setName(String n){
		name = n;
	}
	
	public String getTele(){
		return tele;
	}
	
	public void setTele(String t){
		tele = t;
	}
	
	public boolean getHatBezahlt(){
		return hatBezahlt;
	}
	
	public void setHatBezahlt(boolean b){
		hatBezahlt = b;
	}
}
