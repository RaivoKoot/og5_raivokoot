
public class Schiedsrichter extends Person {

	private int spiele;
	
	public Schiedsrichter(String n, String t, boolean b, int s) {
		super.setName(n);
		super.setTele(t);
		super.setHatBezahlt(b);
		spiele = s;
	}
	
	public int getSpiele(){
		return spiele;
	}
	
	public void setSpiele(int s){
		spiele = s;
	}
}
