
public class Mannschaftsleiter extends Spieler {

	private String mannschaftsName;
	private double rabatt;

	public Mannschaftsleiter(String n, String t, boolean b, String m, Mannschaft ms) {
		super(n, t, b, ms);
		mannschaftsName = m;
	}

	public String getMannschaftsName() {
		return mannschaftsName;
	}

	public void setMannschaftsName(String m) {
		mannschaftsName = m;
	}

	public double getRabbat() {
		return rabatt;
	}

	public void setRabatt(double r) {
		rabatt = r;
	}

}
