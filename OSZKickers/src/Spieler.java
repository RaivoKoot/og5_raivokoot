
public class Spieler extends Person {

	private int trikotNummer;
	private String position;
	Mannschaft mannschaft;
	
	public Spieler(Mannschaft mannschaft, String name){
		this.mannschaft = mannschaft;
		this.mannschaft.addPlayer(this);
		super.setName(name);
		
	}

	public Spieler(String n, String t, boolean b, Mannschaft m) {
		super.setName(n);
		super.setTele(t);
		super.setHatBezahlt(b);
		mannschaft = m;
		mannschaft.addPlayer(this);
	}

	public int getTrikotNummer() {
		return trikotNummer;
	}

	public void setTrikotNummer(int t) {
		trikotNummer = t;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String p) {
		position = p;
	}

	public Mannschaft getMannschaft() {
		return mannschaft;
	}

	public void changeMannschaft(Mannschaft neueMa) {
		boolean success = neueMa.addPlayer(this);
		if (!success) {
			System.out.println("Failed to add player to new Mannschaft");
		} else {
			mannschaft.removePlayer(this);
			mannschaft = neueMa;
			System.out.println("Success");
		}
	}
}
