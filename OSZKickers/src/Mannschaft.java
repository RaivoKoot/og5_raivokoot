
public class Mannschaft {

	private String name;
	private int spielklasse;
	private Spieler[] spielerListe = new Spieler[22];
	private Trainer trainer;

	public Mannschaft(String n, int s, Trainer t) {
		name = n;
		spielklasse = s;
		trainer = t;
	}

	public void setTrainer(Trainer t) {
		trainer = t;
	}

	public Trainer getTrainer() {
		return trainer;
	}

	public void setName(String n) {
		name = n;
	}

	public String getName() {
		return name;
	}

	public void setSpielklasse(int s) {
		spielklasse = s;
	}

	public int getSpielklasse() {
		return spielklasse;
	}

	public void getAllSpielerNames() {
		for (int i = 0; i != 22 && spielerListe[i] != null; i++) {
			System.out.println(spielerListe[i].getName());
		}
	}

	public boolean addPlayer(Spieler spieler) {
		boolean isNotFinished = true;
		int i = 0;
		do {
			if (spielerListe[i] == null) {
				spielerListe[i] = spieler;
				isNotFinished = false;
				return true;
			}
			i++;
		} while (isNotFinished && i != 22);
		return false;
	}

	public void removePlayer(Spieler spieler) {
		boolean isNotFinished = true;

		for (int i = 0; isNotFinished && i != 22; i++) {
			if (spielerListe[i] == spieler) {
				spielerListe[i] = null;
				for (; i != 22 && spielerListe[i + 1] != null; i++) {
					spielerListe[i] = spielerListe[i + 1];
					spielerListe[i + 1] = null;
				}
				isNotFinished = false;
			}
		}
	}

}
