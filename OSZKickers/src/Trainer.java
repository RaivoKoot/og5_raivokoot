
public class Trainer extends Person {

	private char lizenz;
	private int entschaedigung;

	public Trainer(String n, String t, boolean b,char l) {
		super.setName(n);
		super.setTele(t);
		super.setHatBezahlt(b);
		lizenz = l;
	}
	
	public char getLizenz(){
		return lizenz;
	}
	
	public void setLizenz (char l){
		lizenz = l;
	}
	
	public int getEntschaedigung(){
		return entschaedigung;
	}
	
	public void setEntschaedigung(int e){
		entschaedigung = e;
	}
}
