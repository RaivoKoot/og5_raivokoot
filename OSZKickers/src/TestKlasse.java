
public class TestKlasse {

	public static void main(String[] args) {
		Trainer trainer1 = new Trainer("Schleyer", "0158", true, 'b');
		Mannschaft mannschaft1 = new Mannschaft("OSZ Kickers", 3, trainer1);
		Mannschaft mannschaft2 = new Mannschaft("OSZ BALLERS", 2, trainer1);
		Spieler spieler1 = new Spieler("Raivo", "0177", true, mannschaft2);
		Mannschaftsleiter mannschaftsleiter1 = new Mannschaftsleiter("Hafezi", "0157", false, "OSZKicker", mannschaft2);
		Schiedsrichter schiedsrichter1 = new Schiedsrichter("Tenbusch", "0178", true, 60);

		Person[] personen = { spieler1, mannschaftsleiter1, schiedsrichter1, trainer1 };

		for (int i = 0; i < personen.length && !personen[i].equals(null); i++) {
			System.out.println(personen[i].getName());
			System.out.println(personen[i].getTele());
			System.out.println(personen[i].getHatBezahlt());
			System.out.println();
		}

		mannschaft2.getAllSpielerNames();
		System.out.println();

		Spieler[] team1Player = new Spieler[21];
		for (int i = 0; i != 21; i++) {
			team1Player[i] = new Spieler(mannschaft1, "test");
		}
		mannschaftsleiter1.changeMannschaft(mannschaft1);
		mannschaft1.getAllSpielerNames();
		System.out.println();
		mannschaft2.getAllSpielerNames();
	}
}
