import java.text.DecimalFormat;
import java.util.Scanner;

public class MausBestellung {
	public static void main(String[] args) {

		double endBetrag;
		final int SHIPPING = 10;
		final int MIN_FREE_SHIPPING = 10; // Free shipping ab 10
		final double MWST = 1.19;

		double itemPreis;
		int numOfItems;
		Scanner input = new Scanner(System.in);

		System.out.print("Wieviele Maeuse werden bestellt: ");
		numOfItems = input.nextInt();
		input.nextLine();

		System.out.print("Preis Pro Maus exkl. MwsT: ");
		itemPreis = input.nextDouble();
		input.close();

		endBetrag = itemPreis * numOfItems * MWST;
		if (numOfItems < MIN_FREE_SHIPPING) {
			endBetrag += SHIPPING;
		}

		DecimalFormat df = new DecimalFormat("#.##");

		System.out.println("Die Rechnung betraegt " + df.format(endBetrag) + "$");
	}
}
