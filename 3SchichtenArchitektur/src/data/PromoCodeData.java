package data;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class PromoCodeData implements IPromoCodeData {

	private List<String> promocodes;
	
	public PromoCodeData(){
		this.promocodes = new LinkedList<String>();
	}
	
	@Override
	public boolean savePromoCode(String code) {
		
		//File promocodes = new File("./promocodes.txt");
		BufferedWriter bw;
		try {		
		bw = new BufferedWriter(new FileWriter("promocodes.txt", true));
		bw.write(code);
		bw.newLine();
		bw.close();
		}  catch (IOException e){
		System.out.println("ERROR");
		e.printStackTrace();
		
		
	}
		
		
		return this.promocodes.add(code);
	}

	@Override
	public boolean isPromoCode(String code) {
		return this.promocodes.contains(code);
	}

}
