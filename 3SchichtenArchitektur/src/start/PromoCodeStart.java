package start;

import gui.PromocodeFXGUI;
import gui.PromocodeGUI;
import gui.PromocodeTUI;
import javafx.application.Application;
import javafx.stage.Stage;
import logic.IPromoCodeLogic;
import logic.PromoCodeLogic;
import data.IPromoCodeData;
import data.PromoCodeData;

public class PromoCodeStart {

	public static void main(String[] args) {
		IPromoCodeData data = new PromoCodeData();
		IPromoCodeLogic logic = new PromoCodeLogic(data);

	//	new PromocodeTUI(logic);
	//	new PromocodeGUI(logic);
		Application.launch(PromocodeFXGUI.class, args);
	
	}

}
