package gui;

import data.PromoCodeData;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import logic.PromoCodeLogic;

public class PromocodeFXGUI extends Application {

	PromoCodeLogic logic = new PromoCodeLogic(new PromoCodeData());

	@FXML
	Label lbl_code;

	@Override
	public void start(Stage primaryStage) {
		try {
			Parent root = FXMLLoader.load(getClass().getResource("InterfaceDesign.fxml"));
			Scene scene = new Scene(root);

			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void clicked_generateCode() {
		lbl_code.setText(logic.getNewPromoCode());
	}

	public static void main(String[] args) {
		launch(args);
	}
}
