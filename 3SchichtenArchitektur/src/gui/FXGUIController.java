package gui;

import data.PromoCodeData;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import logic.PromoCodeLogic;

public class FXGUIController {

	PromoCodeLogic logic = new PromoCodeLogic(new PromoCodeData());

	@FXML
	Label lbl_code;

	public void clicked_generateCode() {
		lbl_code.setText(logic.getNewPromoCode());
	}

}
