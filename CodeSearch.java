package de.promocode.logic;

public class CodeSearch {

	public static int NOT_FOUND = -1;

	/**
	 * Sucht einen Code in der Liste.
	 * 
	 * @param list
	 *            Liste der Codes im Long-Format
	 * @param searchValue
	 *            Gesuchter Code
	 * @param links
	 *            Argument muss beim Aufrufen aus einer anderen Klasse 0 sein
	 * @param rechts
	 *            Argument muss beim Aufrufen aus einer anderen Klasse der index
	 *            des letzten elementes der Arrays sein
	 * @return Index des Codes, wenn er vorhanden ist, NOT_FOUND wenn er nicht
	 *         gefunden werden konnte
	 */
	public static int suchen(long[] liste, long searchValue, int links, int rechts) {
		if (links <= rechts) {
			int xb = links + (rechts - links) / 2;

			long range = liste[rechts] - liste[links];
			if (range == 0)
				range = 1;
			int xi = (int) (links + ((searchValue - liste[links]) * (rechts - links) / range));

			if (xb > xi) {
				int xbKopie = xb;
				xb = xi;
				xi = xbKopie;
			}
			if (searchValue == liste[xb])
				return xb;
			else if (searchValue == liste[xi])
				return xi;
			else if (searchValue < liste[xb])
				return suchen(liste, searchValue, links, xb - 1);
			else if (searchValue < liste[xi])
				return suchen(liste, searchValue, xb + 1, xi - 1);
			else if (searchValue < liste[rechts])
				return suchen(liste, searchValue, xi + 1, rechts);
		}
		return NOT_FOUND;
	}

}