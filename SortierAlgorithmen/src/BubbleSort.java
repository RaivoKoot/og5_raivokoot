
public class BubbleSort {
	int anzahlVergleiche;
	int anzahlArrayDurchlaeufe;

	public void sortieren(long[] zahlen) {
		System.out.print("Start     { ");
		for (int k = 0; k < zahlen.length; k++)
			System.out.print(zahlen[k] + ", ");
		System.out.println("}");

		for (int i = 0; i < zahlen.length - 1; i++) {
			boolean nichtsGefunden = true;
			for (int j = 0; j < zahlen.length - 1; j++) {
				if (zahlen[j] > zahlen[j + 1]) {
					long tmp = zahlen[j];
					zahlen[j] = zahlen[j + 1];
					zahlen[j + 1] = tmp;
					anzahlVergleiche++;
					System.out.print(j + " <-> " + (j + 1));
					System.out.print("   { ");
					for (int k = 0; k < zahlen.length; k++)
						System.out.print(zahlen[k] + ", ");
					System.out.println("}");
					nichtsGefunden = false;
				}
			}
			anzahlArrayDurchlaeufe++;
			if (nichtsGefunden)
				break;
		}
	}

	public int getAnzahlVergleiche() {
		return anzahlVergleiche;
	}

	public int getAnzahlSchleifen() {
		return anzahlArrayDurchlaeufe;
	}

	public static void main(String[] args) {
		long[] nummern = { 10, 1, 7, 3, 2, 4, 8, 12 };

		BubbleSort b1 = new BubbleSort();
		b1.sortieren(nummern);

		System.out.println("Vergleiche: " + b1.getAnzahlVergleiche());
		System.out.println("Anzahl der Array Durchlaeufe: " + b1.getAnzahlSchleifen());
	}
}
