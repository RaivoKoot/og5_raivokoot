import java.util.Random;
import java.util.Scanner;

public class Testklasse {
	public static void main(String[] args) {
		while (true) {
			// aussuchen wie gross das Array sein soll
			Scanner input = new Scanner(System.in);
			System.out.print("Wie gross soll das Array sein: ");
			int arraylaenge = input.nextInt();
			
			// erstellt ein custom grosses array aus zufaellig
			// grossen int elementen
			Random rand = new Random();
			int[] list = new int[arraylaenge];
			for (int i = 0; i < 100; i++)
				list[i] = rand.nextInt(10000000) + 1;

			
			MergeSort mergeSort = new MergeSort();
			// sortiert die liste
			mergeSort.mergeSort(list);
			// gibt an wie oft in dem sortiervorgang
			// ein element vertauscht wurde
			System.out.print("Elemente wurden ");
			System.out.print(MergeSort.elementeVerschoben);
			System.out.print(" Mal verschoben\n\n");
			MergeSort.elementeVerschoben = 0;
		}
	}
}
