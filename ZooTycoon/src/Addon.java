
public class Addon {

	private int id;
	private String name;
	private double preis;
	private int bestand;
	private int maxBestand;

	public Addon(int id, String name, double preis, int maxBestand) {
		this.id = id;
		this.name = name;
		this.preis = preis;
		this.maxBestand = maxBestand;
		bestand = 0; // default
	}

	public void kaufen() {
		if (bestand < 3) {
			bestand++;
			System.out.println("Addon gekauft");
		} else {
			System.out.println("Kaufen nicht moglich.\nAddon Maxbestand erreicht.");
		}
	}

	public void verbrauchen() {
		if (bestand > 0) {
			bestand--;
		} else {
			System.out.println("Addon nicht in Besitz");
		}
	}

	public double getGesamtWert() {
		double gesamtWert = bestand * preis;
		return gesamtWert;
	}
	

	public int getId() {
		return id;
	}
	
	public void setId(int id){
		this.id = id;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name){
		this.name = name;
	}

	public double getPreis() {
		return preis;
	}
	
	public void setPreis(double preis){
		this.preis = preis;
	}

	public int getBestand() {
		return bestand;
	}
	
	public void setBestand(int bestand){
		if(bestand>=0 && !(bestand > maxBestand)){
		this.bestand = bestand;	
		} else {
			System.out.println("Fehler bei dem Verandern des Bestandes");
		}
	}

	public int getMaxBestand() {
		return maxBestand;
	}
	
	public void setMaxBestand(int maxBestand){
		this.maxBestand = maxBestand;
	}

}
