
public class zooTycoon {

	public static void checkAchievement(double gesamtWert) {
		final double EIN_STERN_AUSZEICHNUNG = 3;
		final double ZWEI_STERN_AUSZEICHNUNG = 5;

		if (gesamtWert >= EIN_STERN_AUSZEICHNUNG) {
			System.out.println("Achievement freigeschaltet" + " 1 Stern Wert Auszeichnung!");
		}
		if (gesamtWert >= ZWEI_STERN_AUSZEICHNUNG) {
			System.out.println("Achievement freigeschaltet" + " 2 Stern Wert Auszeichnung!");
		}
	}

	public static void main(String[] args) {

		Addon goldApfel = new Addon(1, "Goldapfel", 1.99, 3);

		// Kaufe
		System.out.println("Kaufe Addon");
		goldApfel.kaufen();
		int testBestand = goldApfel.getBestand();
		System.out.println("Anzahl dieser Addons im Besitz: " + testBestand);

		// Max Bestand testen
		System.out.println("\nKaufe 3 Addons");
		goldApfel.kaufen();
		goldApfel.kaufen();
		goldApfel.kaufen();

		// Get wert
		double testWert = goldApfel.getGesamtWert();
		System.out.println("\nGesamtwert besitzter Addons: " + testWert);

		// Achievement
		checkAchievement(goldApfel.getGesamtWert());

		// Verbrauche
		System.out.println("\nVerbrauche Addon");
		goldApfel.verbrauchen();
		testBestand = goldApfel.getBestand();
		System.out.println("Anzahl dieser Addons im Besitz: " + testBestand);

		// Achievement
		checkAchievement(goldApfel.getGesamtWert());

	}

}
