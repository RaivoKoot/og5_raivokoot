
public class Interpolationssuche {

	int[] nummern = { 2, 4, 7, 9, 12, 21, 26, 31, 37 };

	public static int suchen(long[] liste, long schluessel) {
		int links = 0;
		int rechts = liste.length - 1;
		long range;
		int position;
		int versuch = 0;

		while (schluessel >= liste[links] && schluessel <= liste[rechts]) {
			versuch++;
			range = liste[rechts] - liste[links];

			position = (int) (links + ((schluessel - liste[links]) * (rechts - links) / range));

			if (schluessel > liste[position]) {
				links = position + 1;
			} else if (schluessel < liste[position]) {
				rechts = position - 1;
			} else {
				System.out.println("versucche " + versuch);
				return position;
			}
		}
		System.out.println("versuche " + versuch);
		return -1;
	}

}