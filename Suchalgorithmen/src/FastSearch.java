public class FastSearch {
	
	
	private static int versuche = 0;
	// Beim Aufrufen dieser Methode muss fuer das Argument links der Parameter 0 sein
	// der Parameter fuer das Argument rechts muss die letzte Elementposition von liste sein
	public static int suchen(long[] liste, long searchValue, int links, int rechts) {
		versuche++;
		if (links <= rechts) {
			int xb = links + (rechts - links) / 2; // Binaere Suchposition

			// Interpolationssuchposition
			long range = liste[rechts] - liste[links];
			if (range == 0) // Damit in der naechsten zeile nicht durch 0 geteilt wird
				range = 1;
			int xi = (int) (links + ((searchValue - liste[links]) * (rechts - links) / range));
			if (xi < 0) { // Wenn negative Arrayposition errechnet wird
				System.out.println("versuche " + versuche);
				versuche = 0;
				return -1;
			} // Interpolationssuchposition fertig

			if (xb > xi) { // Binaere Suchposition immer nach links
				int xbKopie = xb;
				xb = xi;
				xi = xbKopie;
			}
			
			if (searchValue == liste[xb]) {
				System.out.println("versuche: " + versuche);
				versuche = 0;
				return xb;
			} else if (searchValue == liste[xi]) {
				System.out.println("versuche: " + versuche);
				versuche = 0;
				return xi;
			// wenn der Schluessel nicht gefunden ist
			// Methode ruft sich selbst erneut mit kleinerer Elementreichweite auf
			} else if (searchValue < liste[xb])
				return suchen(liste, searchValue, links, xb - 1);
			else if (searchValue < liste[xi])
				return suchen(liste, searchValue, xb + 1, xi - 1);
			else if (searchValue < liste[rechts])
				return suchen(liste, searchValue, xi + 1, rechts);
		}
		System.out.println("versuche " + versuche);
		versuche = 0;
		return -1;
	}
}
