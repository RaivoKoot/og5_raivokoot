public class LinearSuche {

	public static int suchen(long schluessel, long[] nummern) {
		int versuche = 0;
		for (int i = 0; i < nummern.length; i++) {
			versuche++;
			if (schluessel == nummern[i]) {
				System.out.println("versuche " + versuche);
				return i;
			}
		}
		System.out.println("versuche " + versuche);
		return -1;
	}
}
