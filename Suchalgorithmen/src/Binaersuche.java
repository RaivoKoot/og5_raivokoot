
public class Binaersuche {

	public static int suchen(long[] nummern, long schluessel) {
		int position = -1;

		int links = 0;
		int rechts = nummern.length - 1;
		int elementAnzahl;
		int versuche = 1;

		while (schluessel >= nummern[links] && schluessel <= nummern[rechts]) {
			elementAnzahl = rechts - links;
			position = links + elementAnzahl / 2;
			if (schluessel == nummern[position]) {
				System.out.println("versuche " + versuche);
				return position;
			} else if (schluessel > nummern[position]) {
				links = position + 1;
			} else if (schluessel < nummern[position]) {
				rechts = position - 1;
			}
			versuche++;
		}
		System.out.println("versuche " + versuche);
		return -1;
	}

}
