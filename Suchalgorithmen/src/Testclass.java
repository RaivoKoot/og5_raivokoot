import java.util.Random;
import java.util.Scanner;

public class Testclass {

	public static void main(String[] args) {
		while (true) {
			
			
			Scanner input = new Scanner(System.in);
			System.out.print("\nArray Groesse: ");
			int size = input.nextInt(); // Eingabe, wie gross das Array sein soll
			input.nextLine(); 
			long[] nummern = new long[size];

			long rnd = 0;
			Random rand = new Random(1111111); // konstanter Seed fuer konstante Ergebnisse
			for (int i = 0; i < nummern.length; i++) {
				rnd += rand.nextInt(40) + 1; // Das darauffolgende Element immer 
				nummern[i] = rnd;			 // um 1 bis 40 erhoehen
			}

			
			
			
			
			System.out.print("Den Wert welchen Elementes willst du wissen: ");
			int element = input.nextInt();
			input.nextLine();
			System.out.println(nummern[element]);
			
			System.out.print("\nSchluessel: "); // Eingabe, nach welchem Wert
			long schluessel = input.nextLong(); // gesucht werden soll
			input.nextLine();

			System.out.println("FASTSEARCH");
			System.out.println("Element " + FastSearch.suchen(nummern, schluessel, 0, nummern.length - 1));

			System.out.println("\nLINEARSUCHE");
			System.out.println("Element " + LinearSuche.suchen(schluessel, nummern));
		}

	}
}
