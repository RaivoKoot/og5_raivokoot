
public class MergeSortString {
	static int elementeVerschoben;

	public String[] merge(String[] arrayA, String[] arrayB) {
		int a = 0, b = 0; // position des zu vergleichenden Elementes
		// Neue liste die so lange wie beide Arrays zusammen ist
		String[] mergedList = new String[arrayA.length + arrayB.length];

		// Sortiert so oft, wie die Laenge des neuen Arrays
		for (int i = 0; i < mergedList.length; i++) {
			elementeVerschoben++;
			// wenn aus array eins bereits alle elemente verschoben wurden
			if (a == arrayA.length) {
				mergedList[i] = arrayB[b];
				b++;
				// wenn aus array zwei bereits alle elemente verschoben wurde
			} else if (b == arrayB.length) {
				mergedList[i] = arrayA[a];
				a++;
				// wenn element von array 1 kleiner als element von array 2
			} else if (arrayA[a].compareTo(arrayB[b]) < 0) {
				mergedList[i] = arrayA[a];
				a++;
				// wenn element von array 2 kleiner als element von array 1
			} else {
				mergedList[i] = arrayB[b];
				b++;
			}
		}
		return mergedList;
	}

	public String[] mergeSort(String[] array) {
		int length = array.length;
		// wenn array nur ein element hat wird es returned
		if (length < 2)
			return array;

		// zwei neue genau halb so grosse arrays erstellt
		String[] array1 = new String[length / 2];
		String[] array2 = new String[length - array1.length];

		// array 1 die erste haelfte an elementen zuordnen
		for (int i = 0; i < length / 2; i++)
			array1[i] = array[i];
		// array 2 die zweite haelfte an elementen zuordnen
		for (int i = length / 2; i < length; i++)
			array2[i - length / 2] = array[i];
		// beide arrays rekursiv weitergeben und in zwei teilen
		String[] arrayA = mergeSort(array1);
		String[] arrayB = mergeSort(array2);

		// arrays wieder in eines mergen und returnen
		array = merge(arrayA, arrayB);
		return array;
	}

}
