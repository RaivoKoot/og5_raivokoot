import org.apache.commons.lang3.RandomStringUtils;

public class KeyStore01 extends java.lang.Object {

	String[] keys;
	int arrayFuelle = 0;

	public KeyStore01() {
		keys = new String[30000];
	}

	public KeyStore01(int length) {
		keys = new String[length];
	}

	public void clear() {
		int length = keys.length;
		keys = new String[length];
	}

	public boolean add(String e) {
		if (arrayFuelle < keys.length) {
			keys[arrayFuelle] = e;
			arrayFuelle++;
			sortArray(keys);
			return true;
		}
		return false;
	}

	// sortiert das Array selbst wenn nur halb gefuellt
	// erstellt ein temporaeres array das so lang wie alle
	// belegten elemente des keyArrays ist und die 
	// gleichen codes hat. sortiert dieses und setzt es dann
	// wieder in das keyArray ein. Verhindert NullPointerException
	// beim Mergesort wenn ein oder mehr elemente noch kein code
	// zugewiesen wurde (null) 
	public String[] sortArray(String[] unsortedArray) {
		String[] sortedArray = new String[arrayFuelle];
		for (int i = 0; i < arrayFuelle; i++) {
			sortedArray[i] = unsortedArray[i];
		}
		MergeSortString sort = new MergeSortString();
		sortedArray = sort.mergeSort(sortedArray);
		for (int i = 0; i < arrayFuelle; i++) {
			unsortedArray[i] = sortedArray[i];
		}
		return unsortedArray;
	}

	public String get(int index) {
		if (index < keys.length)
			return keys[index];
		return "Array Index out of bounds";
	}

	public int indexOf(String str) {
		// binaersuche
		int position = -1;
		int links = 0;
		int rechts = arrayFuelle - 1;
		int elementAnzahl;
		if (arrayFuelle != 0) {
			while (str.compareTo(keys[links]) >= 0 && str.compareTo(keys[rechts]) <= 0) {
				elementAnzahl = rechts - links;
				position = links + elementAnzahl / 2;
				if (str.equals(keys[position]))
					return position;
				else if (str.compareTo(keys[position]) > 0)
					links = position + 1;
				else if (str.compareTo(keys[position]) < 0)
					rechts = position - 1;
			}
		}
		return position;
	}

	// versetzt alle Elemente die nicht null
	// sind nach links sodass es von links
	// nach rechts keine gaps im array gibt
	public void moveUp(int index) {
		for (int i = index; i < keys.length - 1 && keys[i + 1] != null; i++) {
			keys[i] = keys[i + 1];
			keys[i + 1] = null;
		}
		arrayFuelle--;
	}

	public boolean remove(int index) {
		if (index < keys.length) {
			keys[index] = null;
			moveUp(index);
			return true;
		}
		return false;
	}

	public boolean remove(String str) {
		for (int i = 0; i < keys.length; i++) {
			if (keys[i].equals(str)) {
				keys[i] = null;
				moveUp(i);
				return true;
			}
		}
		return false;
	}

	public int size() {
		return keys.length;
	}

	public String toString() {
		String representation = "";
		for (int i = 0; i < keys.length && i < arrayFuelle; i++) {
			representation += "Key " + i + ": ";
			representation += keys[i];
			representation += "\n";
		}
		return representation;
	}

	// fuellt array mit zufaelligen codes
	// und sortiert diese
	public void fillArray() {
		keys = new String[keys.length];
		arrayFuelle = 0;

		for (int i = 0; i < keys.length; i++) {
			keys[arrayFuelle] = RandomStringUtils.random(10, true, true);
			arrayFuelle++;
		}

		MergeSortString sort = new MergeSortString();
		keys = sort.mergeSort(keys);
	}

}
