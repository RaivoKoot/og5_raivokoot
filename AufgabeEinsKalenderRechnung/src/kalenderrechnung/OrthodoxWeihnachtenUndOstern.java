package kalenderrechnung;

public class OrthodoxWeihnachtenUndOstern {

	// Wann faellt das orthodoxe Weihnachtsfest
	// und der prothestantische Ostersonntag
	// auf den gleichen Tag

	public static String tagBerechnen() {
		// Default Werte
		int x = 2016;
		String monat = "Dezember";
		String monatOstern = "leer";
		int julianischeWeihnachten = 0;
		int os = 0; // Datum des Ostersonntags als Maerztag

		// solange bis Ostertag gleich Weihnachten
		while (!(monat.equals(monatOstern) && julianischeWeihnachten == os)) {
			x++;

			// Umrechnung vom Weihnachten des julianischen Kalenders
			// in den gregorianischen Kalender
			julianischeWeihnachten = 25 + (x / 100) - (x / 400) - 2;

			String[] monatsName = { "Januar", "Februar", "Maerz", "April" };

			int[] monatsTage = { 31, 31, 28, 31 };
			// wenn Tag nicht in den Monat passt
			for (int i = 0; julianischeWeihnachten > monatsTage[i]; i++) {
				monat = monatsName[i];
				julianischeWeihnachten -= monatsTage[i];
			} // Umrechnung fertig

			// Gaussche Osterformel
			// protestantische Ostern
			final int K = x / 100;
			final int M = 15 + (3 * K + 3) / 4 - (8 * K + 13) / 25;
			final int S = 2 - (3 * K + 3) / 4;
			final int A = x % 19;
			final int D = (19 * A + M) % 30;
			final int R = (D + A / 11) / 29;
			final int OG = 21 + D - R;
			final int SZ = 7 - (x + x / 4 + S) % 7;
			final int OE = 7 - (OG - SZ) % 7;
			os = OG + OE;

			monatOstern = "Maerz";
			if (os > 31) {
				os -= 31;
				monatOstern = "April";
			}
		}

		String datum = os + " " + monat + " " + x;
		return datum;
	}
}






