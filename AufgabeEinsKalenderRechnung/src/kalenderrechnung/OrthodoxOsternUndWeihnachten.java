package kalenderrechnung;

public class OrthodoxOsternUndWeihnachten {

	// Wann faellt der orthodoxe Ostersonntag
	// und der prothestantische erste Weihnachtstag
	// auf den gleichen Tag

	public static String tagBerechnen() {
		// Default Werte zuweisen
		String monat = "null";
		int osOst = 0; // orthodoxes Ostern
		int x = 2016;
		int os = 0; // Datum des Ostersonntags als Maerzdatum

		while (!(monat.equals("Dezember") && osOst == 25)) {
			x++;

			// Gausssche Osterformel
			// orthodoxe Ostern
			final int M = 15;
			final int S = 0;
			final int A = x % 19;
			final int D = (19 * A + M) % 30;
			final int R = (D + A / 11) / 29;
			final int OG = 21 + D - R;
			final int SZ = 7 - (x + x / 4 + S) % 7;
			final int OE = 7 - (OG - SZ) % 7;
			os = OG + OE;
			// Osterformel fertig

			// Datum des orthodoxen Ostersonntag vom julianischen in den
			// gregorianischen Kalender
			osOst = os + (x / 100) - (x / 400) - 2;

			// als Maerztag bekommener Tag
			// in den richtigen Monat und Tag umrechnen
			String[] monatsName = { "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November",
					"Dezember", "Januar" };

			// tagesAnzahl der Monate, beginnt mit Maerz
			int[] monatsTage = { 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

			// wenn Tag nicht in den Monat passt
			for (int i = 0; osOst > monatsTage[i]; i++) {
				monat = monatsName[i];
				osOst -= monatsTage[i];
			}
		}
		String datum = osOst + " " + monat + " " + x;
		return datum;
	}
}






