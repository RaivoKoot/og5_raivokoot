package kalenderrechnung;

public class TestKlasse {

	public static void main(String[] args) {

		String datumVonRechnungEins = OrthodoxOsternUndWeihnachten.tagBerechnen();

		System.out.println("Der Orthodoxe Ostersonntag und der erste protestantische");
		System.out.print("Weihnachtstag (25 Dez.) werden am ");
		System.out.println(datumVonRechnungEins);
		System.out.println("(im Gregorianischen Kalender) zum ersten Mal aufeinander treffen.");

		
		String datumVonRechnungZwei = OrthodoxWeihnachtenUndOstern.tagBerechnen();
		
		System.out.println("\n\nDer Orthodoxe erste Weihnachtstag und der protestantische");
		System.out.print("Ostersonntag werden am ");
		System.out.println(datumVonRechnungZwei);
		System.out.println("(im Gregorianischen Kalender) zum ersten Mal aufeinander treffen.");
	}
}
