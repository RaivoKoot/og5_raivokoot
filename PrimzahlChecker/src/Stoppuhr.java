
public class Stoppuhr {

	private static long startpunkt;
	private static long endpunkt;
	
	public static void starten(){
		startpunkt = System.currentTimeMillis();
	}
	
	public static void stoppen(){
		endpunkt = System.currentTimeMillis();
	}
	
	public static double getDauer(){
		double dauer = (double)(endpunkt - startpunkt)/1000;
		return dauer;
	}
	
	public static void reset(){
		startpunkt = 0;
		endpunkt = 0;
	}
}
