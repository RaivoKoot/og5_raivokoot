import java.util.Scanner;

public class CheckPrimzahl {
	private static long zahl;
	private static boolean istPrimzahl = true;
	private static Scanner input = new Scanner(System.in);

	public static void setZahl() {
		System.out.print("Enter a number to check if prime: ");
		zahl = input.nextLong();
	}

	public static void check() {
		for (long i = 2; i < zahl; i++) {
			if (zahl % i == 0) {
				istPrimzahl = false;
			}
		}
	}

	public static void result() {
		System.out.println(zahl + " ist eine Primzahl: " + istPrimzahl);
	}
}
