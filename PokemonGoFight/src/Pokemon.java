import java.io.File;
import java.time.LocalDate;

public class Pokemon {
	private String name;
	private File bild;
	private int wp;
	private int kp;
	private int maxkp;
	private boolean favorit;
	private String typ;
	private double gewicht;
	private double groesse;
	private int bonbons;
	private Attacke[] attacken;
	private LocalDate funddatum;

	public Pokemon() {
	};

	public Pokemon(String name, File bild, int wp, int kp, int maxkp, boolean favorit, String typ, double groesse,
			double gewicht, Attacke[] attacken, LocalDate funddatum) {
		this.setName(name);
		this.setBild(bild);
		this.setWp(wp);
		this.setKp(kp);
		this.setMaxkp(maxkp);
		this.setFavorit(favorit);
		this.setTyp(typ);
		this.setGroesse(groesse);
		this.setGewicht(gewicht);
		this.setAttacken(attacken);
		this.setFunddatum(funddatum);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public File getBild() {
		return bild;
	}

	public void setBild(File bild) {
		this.bild = bild;
	}

	public int getWp() {
		return wp;
	}

	public void setWp(int wp) {
		this.wp = wp;
	}

	public int getMaxkp() {
		return maxkp;
	}

	public void setMaxkp(int maxkp) {
		this.maxkp = maxkp;
	}

	public int getKp() {
		return kp;
	}

	public void setKp(int kp) {
		this.kp = kp;
	}

	public boolean isFavorit() {
		return favorit;
	}

	public void setFavorit(boolean favorit) {
		this.favorit = favorit;
	}

	public String getTyp() {
		return typ;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	public double getGewicht() {
		return gewicht;
	}

	public void setGewicht(double gewicht) {
		this.gewicht = gewicht;
	}

	public double getGroesse() {
		return groesse;
	}

	public void setGroesse(double groesse) {
		this.groesse = groesse;
	}

	public Attacke[] getAttacken() {
		return attacken;
	}

	public void setAttacken(Attacke[] attacken) {
		this.attacken = attacken;
	}

	public LocalDate getFunddatum() {
		return funddatum;
	}

	public void setFunddatum(LocalDate funddatum) {
		this.funddatum = funddatum;
	}

	public int getBonbons() {
		return bonbons;
	}

	public void setBonbons(int bonbons) {
		this.bonbons = bonbons;
	}
}
