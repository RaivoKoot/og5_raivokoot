
public class Attacke {

	private String name;
	private int schaden;

	public Attacke(String name, int schaden){
		this.setName(name);
		this.setSchaden(schaden);
	}

	public int getSchaden() {
		return schaden;
	}

	public void setSchaden(int schaden) {
		this.schaden = schaden;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
