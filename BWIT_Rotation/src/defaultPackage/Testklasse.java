package defaultPackage;

public class Testklasse {

	public static void main(String[] args) {

		DateiLeser dateiLeser = new DateiLeser();
		dateiLeser.readFile();
		
		int lochX = dateiLeser.getLochPositionX();
		Block[] bloecke = dateiLeser.getBloecke();
		int seitenlaenge = dateiLeser.getSeitenlaenge();
		
		// Spielfeldobjekt mit den Dateiinformationen erstellen
		Spielfeld feld = new Spielfeld(seitenlaenge, lochX, bloecke);

		GenerateZuege zuege = new GenerateZuege();
		zuege.generateAndSolve(feld);

	}
}