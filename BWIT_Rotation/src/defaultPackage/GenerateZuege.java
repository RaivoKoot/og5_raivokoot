package defaultPackage;

public class GenerateZuege {

	public void generateAndSolve(Spielfeld originalFeld) {
		int[] drehungen;
		Spielfeld temp; 
		
		for (int zuege = 1; zuege < 28; zuege++) { // Testet jede Moegliche
													// Drehfolge mit bis zu
													// 1 Drehung aus, dann mit
													// bis zu 2, usw. Bis 27
			
			// rechnet aus, wie viele verschiedenen Moeglichkeiten des Drehens
			// es mit 'zuege' Zuegen gibt
			int gesamtDrehfolgen = (int) (Math.pow(2, (double) zuege));

			// Erstellt jede moegliche Drehfolge ein Mal
			for (int drehfolgenNummer = 0; drehfolgenNummer < gesamtDrehfolgen; drehfolgenNummer++) {
				// Kopie des Originalfeldes, an welchem die neu
				// generierte Zugreihenfolge getestet wird
				temp = originalFeld.kopiereSpielfeld();
				drehungen = new int[zuege];

				// wiederholt so oft, bis der Reihenfolge so viele drehungen
				// gegeben wurden, wie zuege gross ist
				for (int wievielteDrehung = 0; wievielteDrehung < zuege; wievielteDrehung++) {

					// Gleichung die vorhin erklaert wurde
					int intervall = gesamtDrehfolgen / (int) Math.pow(2, (double) (wievielteDrehung + 1));
					int spalte = drehfolgenNummer / intervall;
					drehungen[wievielteDrehung] = spalte % 2;
				}

				temp.dreheMitAngabe(drehungen); // testet ob diese Drehangaben
												// zu einer Loesung fuehren
				if (temp.checkWin()) {
					erfolgAusgabe(drehungen, zuege);
					return;
				}
			}
		}
	}

	// stellt informationen zur Loesung dar
	private void erfolgAusgabe(int[] drehungen, int zuege) {
		System.out.println("Erfolg!");
		System.out.println("Zuege: " + zuege);
		System.out.println();
		for (int m = 0; m < drehungen.length; m++) {
			if (drehungen[m] == 0) {
				System.out.print("Links, ");
			} else {
				System.out.print("Rechts, ");
			}
		}
	}
}
