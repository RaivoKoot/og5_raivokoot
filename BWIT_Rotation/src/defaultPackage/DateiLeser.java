package defaultPackage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class DateiLeser {
	Block[] bloecke;
	int gesuchteZahl = 0;
	int seitenlaenge;
	int lochPositionX;

	public void readFile() {
		Scanner input = new Scanner(System.in);
		System.out.println("Dateipfad angeben: ");
		String filePath = input.nextLine();
		input.close();
		File testFile = new File(filePath);
		FileReader fileReader;
		BufferedReader buffReader;
		try {
			// 10 mal datei scannen
			// jedes mal auf der suche nach 0 oder 1 oder 2...
			for (int i = 0; i < 10; i++) {
				fileReader = new FileReader(testFile);
				buffReader = new BufferedReader(fileReader);
				// Puzzlelaenge erfassen
				String line = buffReader.readLine();
				seitenlaenge = Integer.parseInt(line);
				// obersten rahmen uberspringen
				buffReader.readLine();

				// Werte fuer den Block der gesucht wird
				int blockLaenge = 0; // default
				int blockAusrichtung = -1; // default
				int blockY = -1; // default
				int blockX = -1; // default
				boolean istErsterFund = true;

				// Konvertieren von int zu char
				// weil der eingelesene charakter zb 0 oder 1
				// als char eingelesen wird
				char gesuchterBlock = (char) (gesuchteZahl + 48);

				// x und y der for loops dienen um koordinate des momentanen
				// charakters zu wissen
				for (int y = 0; y < seitenlaenge - 1; y++) {
					for (int x = 0; x < seitenlaenge; x++) {

						char character = (char) buffReader.read();
						if (character == gesuchterBlock) {
							if (istErsterFund) {
								blockY = y;
								blockX = x - 1;
								istErsterFund = false;
							}
							// wenn die gesuchte Ziffer ein Zweites
							// mal gefunden wurde
							if (blockLaenge == 1) {
								// je nachdem ob die zweiten Ziffer rechts
								// oder unter dem ersten Fund liegt
								// Ausrichtung bestimmen
								if (y == blockY)
									blockAusrichtung = 3;
								else
									blockAusrichtung = 2;
							}
							blockLaenge++;
						}
						// wenn wir bei dem unteren rahmen angekommen sind
						if (y == seitenlaenge - 2) {
							if (character == ' ') {
								lochPositionX = x - 1;
							}
						}
					}
					buffReader.readLine();
				}

				// nach dem Dateiscan mit den neuen Daten ein Block erstellen
				// und dem Blockarray zuweisen
				if (!istErsterFund)
					addNewBlock(blockLaenge, blockAusrichtung, blockY, blockX);
				gesuchteZahl++;
				fileReader.close();
				buffReader.close();
			}
		} catch (FileNotFoundException e) {
			System.err.println("FileNotFoundException: " + e.getMessage());
		} catch (IOException e) {
			System.err.println("IOException: " + e.getMessage());
		}
	}

	private void addNewBlock(int laenge, int ausrichtung, int y, int x) {
		Block newBlock = new Block(laenge, ausrichtung, y, x);

		// erstellt ein neuen block und dann ein neues array das
		// um eins groesser ist. Dort wird der neue Block eingefuegt
		if (gesuchteZahl != 0) {
			Block[] temp = bloecke;

			bloecke = new Block[temp.length + 1];
			for (int i = 0; i < temp.length; i++) {
				bloecke[i] = temp[i];
			}
			bloecke[temp.length] = newBlock;
		} else {
			bloecke = new Block[1];
			bloecke[0] = newBlock;
		}
	}

	public int getSeitenlaenge() {
		return seitenlaenge - 2;
	}

	public Block[] getBloecke() {
		return bloecke;
	}

	public int getLochPositionX() {
		return lochPositionX;
	}

}
