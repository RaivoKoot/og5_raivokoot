package defaultPackage;

public class Spielfeld {

	private int seitenlaenge;
	private Block[] bloecke;
	private boolean[][] koordinatensystem;
	private int lochAusrichtung;
	private int lochPositionY;
	private int lochPositionX;

	// Zur Initialisierung des Puzzles mit Angaben der Datei
	public Spielfeld(int seitenlaenge, int lochPositionX, Block[] bloecke) {
		this.seitenlaenge = seitenlaenge;
		koordinatensystem = new boolean[seitenlaenge][seitenlaenge];
		lochAusrichtung = 0;
		lochPositionY = seitenlaenge - 1;
		this.lochPositionX = lochPositionX;
		this.bloecke = bloecke;
		spielfeldBesetzen();
		setBloeckeKoordinatensystem();
	}

	// Dient zur Kopierung des Spielfeldes
	private Spielfeld(int seitenlaenge, int lochAusrichtung, int lochPositionY, int lochPositionX) {
		this.seitenlaenge = seitenlaenge;
		koordinatensystem = new boolean[seitenlaenge][seitenlaenge];
		this.lochAusrichtung = lochAusrichtung;
		this.lochPositionY = lochPositionY;
		this.lochPositionX = lochPositionX;
	}

	// Updated frisch besetztes Koordinatensystem der Blockobjekte
	private void setBloeckeKoordinatensystem() {
		for (int i = 0; i < bloecke.length; i++)
			bloecke[i].setKoordinatensystem(koordinatensystem);
	}

	// besetzt das Koordinatensystem mit TRUE an stellen, wo ein Block liegt
	private void spielfeldBesetzen() {
		for (int i = 0; i < bloecke.length; i++) {
			for (int j = 0; j < bloecke[i].getPositionsNummern().length; j += 2) {
				int y = bloecke[i].getPositionsNummern()[j];
				int x = bloecke[i].getPositionsNummern()[j + 1];
				koordinatensystem[y][x] = true;
			}
		}
	}

	// resetted das Koordinatensystem, als waere es unbesetzt
	private void clearFeld() {
		koordinatensystem = new boolean[seitenlaenge][seitenlaenge];
	}

	// dreht alle Bloecke und das Loch nach links, als wuerde man das Puzzle
	// drehen
	private void linksDrehen() {
		for (int i = 0; i < bloecke.length; i++)
			bloecke[i].nachLinksDrehen();
		lochNachLinksDrehen();
		bloeckeFallen();
	}

	// dreht alle Bloecke und das Loch nach rechts, als wuerde man das Puzzle
	// drehen
	private void rechtsDrehen() {
		for (int i = 0; i < bloecke.length; i++)
			bloecke[i].nachRechtsDrehen();
		lochNachRechtsDrehen();
		bloeckeFallen();
	}

	// laesst Bloecke, unter denen Platz frei ist, nach unten rutschen.
	// wird benutzt nachdem die Bloecke gedreht wurden
	private void bloeckeFallen() {
		boolean incomplete = true;
		clearFeld();
		spielfeldBesetzen();
		setBloeckeKoordinatensystem();
		while (incomplete) {
			incomplete = false;
			for (int i = 0; i < bloecke.length; i++) {
				if (true)
					;
				if (bloecke[i].istPlatzUnterBlockFrei()) {
					bloecke[i].blockUmEinsFallen();
					incomplete = true;
				}
				clearFeld();
				spielfeldBesetzen();
				setBloeckeKoordinatensystem();
			}
		}
	}

	// prueft, ob ein Block aus dem Feld herausrutschen kann
	public boolean checkWin() {
		if (lochAusrichtung == 0)
			for (int i = 0; i < bloecke.length; i++) {
				if (bloecke[i].kannRausfallen(lochPositionY, lochPositionX))
					return true;
			}
		return false;
	}

	// veraendert Koordinaten des Lochs, als wuerde man das Feld nach rechts
	// drehen
	private void lochNachRechtsDrehen() {
		int lochYGespeichert = lochPositionY;
		lochPositionY = lochPositionX;
		lochPositionX = koordinatensystem.length - 1 - lochYGespeichert;
		if (lochAusrichtung == 0) {
			lochAusrichtung = 3;
		} else {
			lochAusrichtung--;
		}
	}

	// veraendert Koordinaten des Lochs, als wuerde man das Feld nach links
	// drehen
	private void lochNachLinksDrehen() {
		int lochXGespeichert = lochPositionX;
		lochPositionX = lochPositionY;
		lochPositionY = koordinatensystem.length - 1 - lochXGespeichert;
		if (lochAusrichtung == 3) {
			lochAusrichtung = 0;
		} else {
			lochAusrichtung++;
		}
	}

	// dreht das Feld drehungen.length anzahl an Male mit
	// Richtungsangabe aus dem array
	public void dreheMitAngabe(int[] drehungen) {
		for (int i = 0; i < drehungen.length; i++) {
			if (drehungen[i] == 0)
				linksDrehen();
			if (drehungen[i] == 1)
				rechtsDrehen();
		}
	}

	//
	// Klonmethoden
	//
	public Spielfeld kopiereSpielfeld() {
		Spielfeld duplikat = new Spielfeld(seitenlaenge, lochAusrichtung, lochPositionY, lochPositionX);
		duplikat.setFeld(kopiereKoordinatensystem());
		duplikat.setBloecke(kopiereBloecke());
		duplikat.setBloeckeKoordinatensystem();
		return duplikat;
	}

	private boolean[][] kopiereKoordinatensystem() {
		boolean[][] kopie = new boolean[koordinatensystem.length][koordinatensystem.length];
		for (int i = 0; i < kopie.length; i++)
			for (int k = 0; k < kopie.length; k++)
				kopie[i][k] = koordinatensystem[i][k];
		return kopie;
	}

	public void setFeld(boolean[][] kopie) {
		koordinatensystem = kopie;
	}

	private Block[] kopiereBloecke() {
		Block[] bloeckeDuplikat = new Block[bloecke.length];
		for (int i = 0; i < bloecke.length; i++) {
			bloeckeDuplikat[i] = bloecke[i].kopiereBlock();
		}
		return bloeckeDuplikat;
	}

	public void setBloecke(Block[] bloeckeKopie) {
		bloecke = bloeckeKopie;
	}
}
