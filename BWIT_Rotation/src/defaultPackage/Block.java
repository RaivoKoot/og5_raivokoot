package defaultPackage;

public class Block {

	private int[] positionsNummern;
	private final int laenge;
	private int ausrichtung; // 0 nach oben, 1 links, 2 unten, 3 rechts
	private int y;
	private int x;
	private boolean[][] koordinatenystem;

	public Block(int laenge, int ausrichtung, int y, int x) {
		this.laenge = laenge;
		this.ausrichtung = ausrichtung;
		this.y = y;
		this.x = x;
		setPosition();
	}

	// updated koordinaten mit neuveraenderten X und Y
	
	private void setPosition() {
		int yKopie = y;
		int xKopie = x;

		// Erstkoordinate wird zum Koordinatenarray hinzugefuegt
		positionsNummern = new int[(laenge * 2)];
		positionsNummern[0] = y;
		positionsNummern[1] = x;
		
		for (int i = 2; i < positionsNummern.length - 1; i += 2) {
			// je nach ausrichtung wird die naechste Koordinate
			// richtig bestimmt
			switch (ausrichtung) {
				case 0: y--; break;
				case 1: x--; break;
				case 2: y++; break;
				case 3: x++; break;
			} 
			positionsNummern[i] = y;
			positionsNummern[i + 1] = x;
		}
		// y und x werden fuer das naechste drehen wieder
		// auf die werte der Erstkoordinate gestellt 
		y = yKopie;
		x = xKopie;
	}

	// veraendert X, Y und Ausrichtung , als wuerde der Block sich nach links
	// drehen
	
	public void nachLinksDrehen() {
		// bewegt die Erstkoordinate so,
		// als wuerde sich das Feld nach links drehen
		int xGespeichert = x;
		x = y;
		y = koordinatenystem.length - 1 - xGespeichert;
		// veraendert gemaess die ausrichtung
		if (ausrichtung == 3) {
			ausrichtung = 0;
		} else {
			ausrichtung++;
		}
		setPosition();
	}

	// veraendert X, Y und Ausrichtung , als wuerde der Block sich nach rechts
	// drehen
	public void nachRechtsDrehen() {
		int yGespeichert = y;
		y = x;
		x = koordinatenystem.length - 1 - yGespeichert;
		if (ausrichtung == 0) {
			ausrichtung = 3;
		} else {
			ausrichtung--;
		}
		setPosition();
	}

	// ueberprueft das boolean[][] nach freiem platz
	public boolean istPlatzUnterBlockFrei() {
		int y;
		int x;
		for (int i = 0; i < positionsNummern.length; i += 2) {
			switch (ausrichtung) {
			// wenn ausrichtung horizontal
			case 1:
			case 3:
				y = positionsNummern[i] + 1;
				x = positionsNummern[i + 1];
				// ueberprueft ob unter einer koordinate des Staebchens platz
				// frei ist wenn nicht dann returned sofort false. wenn ja laeuft
				// die schleife fuer jede weiter koordinate je ein mal
				if (y == koordinatenystem.length || koordinatenystem[y][x])
					return false;
				break;
				// wenn ausrichtung vertikal
				// uberprueft ob unter der untersten koordinate des 
				// staebchens platz frei ist
				// wenn ja returned sofort true
			case 0:
				y = positionsNummern[0] + 1;
				x = positionsNummern[1];
				if (y == koordinatenystem.length || koordinatenystem[y][x])
					return false;
				else
					return true;
			case 2:
				y = positionsNummern[positionsNummern.length - 2] + 1;
				x = positionsNummern[1];
				if (y == koordinatenystem.length || koordinatenystem[y][x])
					return false;
				else
					return true;
			}
		}
		// gelangt hierher wenn der stab horizontal liegt
		// und unter jeder koordinate platz ist
		return true;
	}

	// versetzt die Position (koordinaten) um genau eins nach unten
	public void blockUmEinsFallen() {
		for (int i = 0; i < positionsNummern.length; i += 2)
			positionsNummern[i] += 1;
		y++;
	}

	// wenn der block vertikal steht --> uberprueft ob der unterste Teil des Blocks
	// die gleichen Koordinaten, wie das Loch hat
	public boolean kannRausfallen(int lochPositionY, int lochPositionX) {
		int y = -1;
		int x = -1;
		if (ausrichtung == 0 || ausrichtung == 2) {
			switch (ausrichtung) {
			case 2:
				y = positionsNummern[positionsNummern.length - 2];
				x = positionsNummern[1];
				break;
			case 0:
				y = positionsNummern[0];
				x = positionsNummern[1];
				break;
			}
			if (y == lochPositionY && x == lochPositionX) {
				return true;
			}
		}
		return false;

	}

	// Klonmethode
	public Block kopiereBlock() {
		Block blockKopie = new Block(laenge, ausrichtung, y, x);
		return blockKopie;
	}

	public int[] getPositionsNummern() {
		return positionsNummern;
	}

	public void setKoordinatensystem(boolean[][] koordinatenystem) {
		this.koordinatenystem = koordinatenystem;
	}

}
